/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package colecciones;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.sound.midi.MidiMessage;
import utilidades.ES;

/**
 *
 * @author luisnavarro Las opciones del menú son 1 Leer clientes de BBDD 2
 * Añadir cliente 3 Borrar Cliente 4 Modificar Cliente 5 Listar Clientes 6 Salir
 */
public class UIGestorClientes {
    private static GestorClientes miGestorClientes;
    public static void main(String[] args) {
        miGestorClientes= new GestorClientes();
        int opcion;
        //Nombres de las asignaturas en un ArrayList
        //ArrayList<Cliente> listaClientes =new ArrayList<>();
        //Map<Integer,Cliente> mapaClientes=new HashMap<>();

        do {
            opcion = mostrarMenú();
            switch (opcion) {
                case 1:
                    leeClientesDeBBDD();
                    break;
                case 2:
                    addCliente();
                    break;
                case 3:
                    norrarCliente();
                    break;
                case 4:
                    modificarCliente();
                    break;
                case 5:
                    listarClientes();
                    break;
                case 6:
                    System.out.println("Saliendo del programa...");
                    break;
                default:
                    System.out.println("Opción Incorrecta.");
            }
        } while (opcion != 6);
        
        System.out.println("Fin del programa.");
   
    }
    private static void norrarCliente(){
        //opción no implementada
    }
    private static void modificarCliente(){
        //opción no implementada        
    }
    private static void listarClientes(){
        //opción no implementada        
    }
    private static void addCliente(){
        //opción no implementada        
    }
    private static void leeClientesDeBBDD(){
        miGestorClientes.addClientesDesdeBBDD();
        //ArrayList<Cliente> listaClientes = GestorClientes.leeClientesDeBBDD_simulador(); //new ArrayList<>());
        //Ahora añado esta ArrayList al mapa de clientes

    }
    private static int mostrarMenú() {
        System.out.println("1.- Leer clientes de BBDD");
        System.out.println("2.- Añadir Cliente");
        System.out.println("3.- Borrar Cliente");
        System.out.println("4.- Modificar Cliente");
        System.out.println("5.- Listar Clientes");
        System.out.println("6.- Salir");
        System.out.print("Elija opción: ");
        int opcion = ES.leeEntero("Elija opción", 1, 6);
        return opcion;
    }

}
