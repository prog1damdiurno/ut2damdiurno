/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package colecciones;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author luisnavarro
 */
public class GestorClientes {
    private Map<Integer,Cliente> mapaClientes;
//En la clase GestorClientes habrá un método simulador:
    public GestorClientes(){
        mapaClientes=new HashMap<>();
    }
    public void addClientesDesdeBBDD(){
        ArrayList<Cliente> listaClientes = GestorClientes.leeClientesDeBBDD_simulador(); //new ArrayList<>());
    }
    public static ArrayList<Cliente> leeClientesDeBBDD_simulador() {
        ArrayList<Cliente> clientes = new ArrayList<>();

        // Agregando 8 clientes al ArrayList
        clientes.add(new Cliente("12345678A", "García, Juan", LocalDate.of(1980, 5, 15), "juan.garcia@example.com", "600100200"));
        clientes.add(new Cliente("23456789B", "López, María", LocalDate.of(1990, 8, 25), "maria.lopez@example.com", "600200300"));
        clientes.add(new Cliente("34567890C", "Martínez, Carlos", LocalDate.of(1975, 3, 10), "carlos.martinez@example.com", "600300400"));
        clientes.add(new Cliente("45678901D", "Sánchez, Ana", LocalDate.of(1985, 12, 20), "ana.sanchez@example.com", "600400500"));
        clientes.add(new Cliente("56789012E", "Morales, Luis", LocalDate.of(1995, 7, 30), "luis.morales@example.com", "600500600"));
        clientes.add(new Cliente("67890123F", "Gómez, Diana", LocalDate.of(1978, 6, 5), "diana.gomez@example.com", "600600700"));
        clientes.add(new Cliente("78901234G", "Pérez, Jorge", LocalDate.of(1982, 9, 15), "jorge.perez@example.com", "600700800"));
        clientes.add(new Cliente("89012345H", "Rodríguez, Sofia", LocalDate.of(1992, 11, 25), "sofia.rodriguez@example.com", "600800900"));

        // Imprimiendo la información de cada cliente, esto debería hacerse en el método que llama a este
        for (Cliente cliente : clientes) {
            System.out.println(cliente.toString());
        }
	return clientes;
    }
}


