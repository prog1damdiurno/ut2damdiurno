package colecciones;

import colecciones.ClaudiaAlumno;
import java.util.ArrayList;
import java.util.Scanner;

public class ClaudiaEjercicio1 {

    static Scanner teclado = new Scanner(System.in);
    //Creamos un ArrayList de la clase Alumno
    static ArrayList<ClaudiaAlumno> alumnos = new ArrayList<ClaudiaAlumno>();
    static ArrayList<String> nombreAsignaturas = new ArrayList<String>();

    public static void main(String[] args) {

        int opcion;
        //Nombres de las asignaturas en un ArrayList
        rellenarNombresAsignaturas();

        do {
            opcion = mostrarMenú();
            switch (opcion) {
                case 1:
                    IngresarDatosAlumno();
                    break;
                case 2:
                    MostrarInformaciónAlumnos();
                    break;
                case 3:
                    IngresarNotasPorAsignatura();
                    break;
                case 4:
                    mostrarPromedioAlumnos();
                    break;
                case 5:
                    mostrarPromedioAsignaturas();
                    break;
                case 6:
                    System.out.println("Saliendo del programa...");
                    break;
                default:
                    System.out.println("Opción Incorrecta.");
            }
        } while (opcion != 6);
        
        System.out.println("Fin del programa.");
    }

    //Muestra el menú y pregunta al usuario la opción
    static int mostrarMenú() {
        System.out.println("1.- Introducir información alumno");
        System.out.println("2.- Mostrar información alumno");
        System.out.println("3.- Poner nota alumno");
        System.out.println("4.- Promedio alumno");
        System.out.println("5.- Promedio asignatura");
        System.out.println("6.- Salir");
        System.out.print("Elija opción: ");
        int opcion = teclado.nextInt();
        teclado.nextLine(); //Limpiar el salto de línea
        System.out.println();
        return opcion;
    }

    //Opción 1. Permite ingresar los datos del alumno
    static void IngresarDatosAlumno() {
        //Pedimos los datos al usuario
        System.out.print("Introduzca el nombre del alumno: ");
        String nombre = teclado.nextLine();
        System.out.print("Introduzca los apellidos del alumno: ");
        String apellidos = teclado.nextLine();
        System.out.print("Introduzca la dirección del alumno: ");
        String direccion = teclado.nextLine();
        System.out.print("Introduzca el teléfono del alumno: ");
        String telefono = teclado.nextLine();

        //Creamos el objeto alumno
        ClaudiaAlumno alumnoNuevo = new ClaudiaAlumno(nombre, apellidos, direccion, telefono);

        //Añadimos el objeto alumno al arraylist
        alumnos.add(alumnoNuevo);
        int posicion = alumnos.indexOf(alumnoNuevo);
        System.out.println("Se ha añadido al alumno en la posición " + posicion);
        System.out.println();
    }

    //Opción 2. Mostrar la información de los alumnos
    static void MostrarInformaciónAlumnos(){
        for(ClaudiaAlumno al : alumnos){
            System.out.println(al);
        }
        System.out.println();
    }
    
    //Opción 3. Ingresa la nota de una asignatura en un alumno
    static void IngresarNotasPorAsignatura(){
        //Pedimos la información del alumno al usuario
        System.out.print("Introduzca el nombre del alumno: ");
        String nombre = teclado.nextLine();
        System.out.print("Introduzca los apellidos: ");
        String apellidos = teclado.nextLine();
        
        //Buscamos al alumno en el ArrayList
        int posicion = buscarPosicionAlumno(nombre, apellidos);
        
        if(posicion != -1){
            //Pedimos la nota del alumno
            System.out.println("Asignaturas:");
            int contador = 1;
            //Mostramos la lista de asignaturas numeradas
            for(int i=0;i<nombreAsignaturas.size();i++){
                System.out.println((contador++)+ ".- " + nombreAsignaturas.get(i));
            }
            //Pedimos la asignatura
            System.out.print("Introduzca el número de la asignatura: ");
            int opcion = teclado.nextInt();
            while(opcion<1||opcion>4){
                //Aseguramos que el usuario ha introducido una opción correcta
                System.out.println("Opción Incorrecta. Debe ser entre 1 y 4.");
                System.out.print("Introduzca el número de la asignatura: ");
                opcion = teclado.nextInt();
            }
            //Pedimos la nota
            System.out.print("Introduzca la nota: ");
            double nota = teclado.nextDouble();
            while(nota<0 || nota > 10){
                //Aseguramos que el usuario ha introducido una nota correcta
                System.out.println("Nota incorrecta. Debe ser entre 0 y 10.");
                System.out.print("Introduzca la nota: ");
                nota = teclado.nextDouble();
            }
            teclado.nextLine();
     
            //Guardamos la nota en el ArrayList en la posición de la asignatura
            alumnos.get(posicion).setNotas((opcion - 1), nota);
            
        }else{
            //Si el alumno no existe mostramos un mensaje de error
            System.out.println("El alumno indicado no existe.");
        }
        System.out.println();
    }
    
    //Busca el alumno en el ArrayList según nombre y apellidos
    private static int buscarPosicionAlumno(String nombre, String apellidos){
         for (int i = 0; i < alumnos.size(); i++) {
             if(alumnos.get(i).getNombre().equalsIgnoreCase(nombre)
                     && alumnos.get(i).getApellidos().equalsIgnoreCase(apellidos)){
                 return i;
             }
         
         }
         //Si no se encuentra, devuelve -1
         return -1;
    }
    
    //Opción 4. Mostrar el promedio de notas de todos los alumnos
    static void mostrarPromedioAlumnos(){
        System.out.println("Promedios alumnos: ");
        //Calculamos el promedio de cada alumno
        for (int i = 0; i < alumnos.size(); i++) {
            ClaudiaAlumno alumno = alumnos.get(i);
            int sumaNotas = 0;
            int cantidadAsignaturasEvaluadas = 0;
            for (int j = 0; j < alumno.getNotas().size(); j++) {
                if (alumno.getNotas().get(j) != null) {
                    sumaNotas += alumno.getNotas().get(j);
                    cantidadAsignaturasEvaluadas++;
                }
            }
            if (cantidadAsignaturasEvaluadas > 0) {
                double promedio = (double) sumaNotas / cantidadAsignaturasEvaluadas;
                //Mostramos el promedio
                System.out.printf("%s: %.2f%n", alumno.getNombre(), promedio);
            } else {
                System.out.println(alumno.getNombre() + ": No hay asignaturas evaluadas.");
            }
        }
        System.out.println();
    }
    
    //Opción 5. Mostrar el promedio por asignatura
    static void mostrarPromedioAsignaturas() {
        System.out.println("Promedios asignaturas: ");
        //Calculamos el promedio de cada asignatura
        for (int i = 0; i < nombreAsignaturas.size(); i++) {
            int sumaNotas = 0;
            int cantidadNotasEvaluadas = 0;
            for (int j = 0; j < alumnos.size(); j++) {
                ClaudiaAlumno alumno = alumnos.get(j);
                if (alumno.getNotas().get(i) != null) {
                    sumaNotas += alumno.getNotas().get(i);
                    cantidadNotasEvaluadas++;
                }
            }
            if(cantidadNotasEvaluadas > 0){
                double promedio = (double) sumaNotas / cantidadNotasEvaluadas;
                //Mostramos el promedio
                System.out.printf("%s: %.2f%n", nombreAsignaturas.get(i), promedio);
            }else{
                System.out.println(nombreAsignaturas.get(i)+": No hay notas evaluadas.");
            }
        }
        System.out.println();
    }
    
    //Guarda los nombres de las asignaturas en un ArrayList
    public static void rellenarNombresAsignaturas(){
        nombreAsignaturas.add("PRO");
        nombreAsignaturas.add("DB");
        nombreAsignaturas.add("LMSI");
        nombreAsignaturas.add("SYS");
    }
    
}
