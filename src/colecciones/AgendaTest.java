
package colecciones;
import java.util.Scanner;


public class AgendaTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Agenda myAgenda = new Agenda();
        Scanner sc = new Scanner(System.in);
        Scanner sct = new Scanner(System.in);
        String nombre;
        int option = 0;


            do {
                System.out.println("1.- Añadir un contacto");
                System.out.println("2.- Borrar un contacto");
                System.out.println("3.- Buscar un contacto");
                System.out.println("4.- Listado de contactos");
                System.out.println("5.- Ordenar contactos por nombre");
                System.out.println("6.- Ordenar contactos por teléfono");
                System.out.println("7.- Ordenar contactos por dirección");
                System.out.println("8.- Salir");
                System.out.print("Elige una opción ");
                option = sc.nextInt();
                switch (option) {
                    case 1:
                        System.out.print("Nombre del contacto: ");
                        nombre = sct.nextLine();
                        System.out.print("Teléfono: ");
                        String telefono = sct.nextLine();
                        System.out.print("Dirección: ");
                        String direccion = sct.nextLine();
                        myAgenda.añadirContacto(new Contacto(nombre,
                                telefono,direccion));
                        break;
                    case 2:
                        System.out.print("Nombre del contacto: ");
                        nombre = sct.nextLine();
                        if (myAgenda.borrarContacto(nombre)) {
                            System.out.println("Este contacto ha "
                                    + "sido eliminado");
                        } else {
                            System.out.println("El contacto no existe");
                        }
                        break;
                    case 3:
                        System.out.print("Nombre del contacto: ");
                        nombre = sct.nextLine();
                        Contacto c = myAgenda.buscarContacto(nombre);
                        if (c != null) {
                            System.out.println(c);
                        } else {
                            System.out.println("El contacto no existe");
                        }
                        break;
                    case 4:
                        myAgenda.listarContactos();
                        break;
                    case 5:
           
                       // myAgenda.ordenarAgenda();
                        myAgenda.listarContactos();
                        break;
                    case 6:
                      
                      //  myAgenda.ordenarPorTelefono();
                        myAgenda.listarContactos();
                        break;
                        
                        case 7:
                       
                       // myAgenda.ordenarPorDireccion();
                        myAgenda.listarContactos();
                        break;
                    case 8:
                        for (Contacto contact : myAgenda.getAgenda()) {
                            System.out.println(contact);
                        }
                        System.out.println("Adiós");
                        break;
                    default:
                        System.out.println("Opción incorrecta");

                }
            } while (option != 6);

    }
}
