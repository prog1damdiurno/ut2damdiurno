
package colecciones;

import java.util.Collections;
import java.util.LinkedList;

public class Agenda {
    LinkedList <Contacto>agenda;
    
    public Agenda() {
        agenda=new LinkedList();
    }

    public LinkedList<Contacto> getAgenda() {
        return agenda;
    }  

    public void setAgenda(LinkedList<Contacto> agenda) {
        this.agenda = agenda;
    }
    
    
    public boolean añadirContacto(Contacto c) {
        return agenda.add(c);
    }
    
    public Contacto buscarContacto(String nombre) {        
        for (Contacto contacto:agenda) {
           if (contacto.getNombre().equalsIgnoreCase(nombre)) 
               return contacto;
        }
        return null;
    }
    
    public boolean borrarContacto(String nombre) {
        Contacto c = buscarContacto(nombre);
        if(c!=null){
        if (agenda.contains(c)) {
            agenda.remove(c);
            return true;
        }
        }
        return false;
    }
    
    public void listarContactos() {
        for (Contacto c:agenda) {
            System.out.println(c);
        }
    } 
    public void ordenarAgenda()
    {
        //¿Cómo ordeno la agenda
    }
      
}
