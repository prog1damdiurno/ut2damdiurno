

package colecciones;

import java.util.*;


public class Empleado {
    private String nombre;
    private int edad;
    private double salario;
    private Date diaAlta;
    
    public Empleado(String nombre, int edad, double salario){//, Date da){
        this.nombre=nombre;
        this.edad=edad;
        this.salario=salario;
        //diaAlta=da;
    }
    
    public String dameDatos(){
        return "El empleado se llama "+nombre+". Tiene "+edad+" años"+" y un salario de "+salario+" y día de alta="+diaAlta;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.nombre);
        hash = 83 * hash + this.edad;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        if (this.edad != other.edad) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
 
    
}
