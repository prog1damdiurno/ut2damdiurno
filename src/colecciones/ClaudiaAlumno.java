package colecciones;

import java.util.ArrayList;

public class ClaudiaAlumno {

    // ATRIBUTOS
    private String nombre;
    private String apellidos;
    private String direccion;
    private String telefono;
    final int ASIGNATURAS = 4;
    private ArrayList<Double> notas = new ArrayList<Double>();

    // CONSTRUCTOR
    public ClaudiaAlumno(String nombre, String apellidos, String direccion, String telefono) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        //Inicializamos las notas a -1
        this.notas = inicializarNotas();
    }

    @Override
    public String toString() {
        return "Alumno:"
                + "\n\t- Nombre: " + this.nombre
                + "\n\t- Apellidos: " + this.apellidos
                + "\n\t- Direccién: " + this.direccion
                + "\n\t- Teléfono: " + this.telefono
                + "\n\t- Notas: " + mostrarNotas();
    }

    //Muestra NE si no existe nota
    private String mostrarNotas() {
        String notas = "";
        int tamaño = this.notas.size();
        for (int i = 0; i < tamaño; i++) {
            if (this.notas.get(i) == null) {
                notas += "NE ";
            } else {
                notas += this.notas.get(i) + " ";
            }
        }
        return notas;
    }

    //Inicializa las notas de cada asignatura a null
    private ArrayList<Double> inicializarNotas() {
        ArrayList<Double> inicializarNota = new ArrayList<>();
        for (int i = 0; i < ASIGNATURAS; i++) {
            inicializarNota.add(null);
        }
        return inicializarNota;
    }

    // MÉTODOS SETTER
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    //Permite cambiar la nota en una posición concreta
    public void setNotas(int posicion, double nuevoValor) {
        this.notas.set(posicion, nuevoValor);
    }

    // MÉTODOS GETTER
    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public ArrayList<Double> getNotas() {
        return notas;
    }

}
