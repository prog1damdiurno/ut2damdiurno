/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package colecciones;


import java.time.LocalDate;
import java.util.*;

/**
 *
 * @author luisnavarro Se trata de leer una serie de clientes en una ArrayList y
 * hacer un mapa con ellos, como clave el DNI y valor el cliente
 */
public class PruebaMapas1 {

    public static void main(String[] args) {
        ArrayList<Cliente> miListaClientes;//=new ArrayList<>():
        miListaClientes = GestorClientes.leeClientesDeBBDD_simulador();
        //Hemos de iterar esta lista y añadirla a un mapa
        System.out.println("mi lista clientes:" + miListaClientes);
        Map<String, Cliente> miMapaClientes = new HashMap<>();
        Cliente ejemploCliente;
        //ejemploCliente.getDni();
        //Añadimos por 

        //recorro la lista y añado al mapa
        Iterator it=miListaClientes.iterator();
        
        while(it.hasNext()){
            Cliente cli=(Cliente)it.next();
            miMapaClientes.put(cli.getDni(),cli);
        }
        
        //Conjunto de claves
        Set conjuntoClaves = miMapaClientes.keySet();
        System.out.println("mi mapa clientes:" + miMapaClientes);

        System.out.println("mi conjunto de claves:" + miMapaClientes);
        System.out.println("mi conjunto de claves:" + miMapaClientes);
        Cliente elPrimeroDeLaLista=miListaClientes.get(1);
        //Borramos un cliente de la lista por clave...dni=67890123F que corresponde a Diana Gómez
        
        //Borramos un cliente de la lista por valor: elPrimeroDeLaLista

        //Imprimir coleccion de valores
        Collection coleccionClientes=miMapaClientes.values();
        
        //Voy a añadir a dos clientes, pero compruebo si existen, si existen digo que lo reemplazo, si no que nuevo
        Cliente ClienteNuevo1=new Cliente("56789012E", "Morales Pez, Luis", LocalDate.of(1995, 7, 20), "luis.morales2@example.com", "600500600");
        Cliente ClienteNuevo2=new Cliente("44789012E", "Morales Pez, Manolo", LocalDate.of(1995, 7, 20), "manolo.morales2@example.com", "600500601");
        //Borramos el cliente
        //Borramos todo el mapa
        //comprobamos que está vacío...
    }
}
