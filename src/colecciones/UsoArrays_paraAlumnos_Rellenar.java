/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package colecciones;

/**
 *
 * @author luisnavarro: Ejercicio preliminar de ut6 colecciones para ver métodos hechos que manejan los arrays.
 */
import java.util.Arrays;

public class UsoArrays_paraAlumnos_Rellenar {

    private int arregloInt[] = {1, 2, 3, 4, 5, 6};
    private double arregloDouble[] = {8.4, 9.3, 0.2, 7.9, 3.4};
    private int arregloIntLleno[], copiaArregloInt[];
// el constructor inicializa los arreglos

    public UsoArrays_paraAlumnos_Rellenar() {
        arregloIntLleno = new int[10]; // crea arreglo int con 10 elementos
        copiaArregloInt = new int[arregloInt.length];
        Arrays.fill(arregloIntLleno, 7); // llena con 7s
        Arrays.sort(arregloDouble); // ordena arregloDouble en forma ascendente
// copia el arreglo arregloInt en el arreglo copiaArregloInt
        System.arraycopy(arregloInt, 0, copiaArregloInt, 0, arregloInt.length);
    } // fin del constructor de UsoArrays
// imprime los valores en cada arreglo

    public void imprimirArreglos() {
        System.out.print("arregloDouble: ");
        for (double valorDouble : arregloDouble) {
            System.out.printf("%.1f ", valorDouble);
        }
        System.out.print("\narregloInt: ");
        for (int valorInt : arregloInt) {
            System.out.printf("%d ", valorInt);
        }
        System.out.print("\narregloIntLleno: ");
        for (int valorInt : arregloIntLleno) {
            System.out.printf("%d ", valorInt);
        }
        System.out.print("\ncopiaArregloInt: ");
        for (int valorInt : copiaArregloInt) {
            System.out.printf("%d ", valorInt);
        }
        System.out.println("\n");
    } // fin del método imprimirArreglos
// busca un valor en el arreglo arregloInt

    public int buscarUnInt(int valor) {
        return Arrays.binarySearch(arregloInt, valor);
    } // fin del método buscarUnInt
// compara el contenido del arreglo

    public void imprimirIgualdad() {
        boolean b = Arrays.equals(arregloInt, copiaArregloInt);
        System.out.printf("arregloInt %s copiaArregloInt\n", (b ? "==" : "!="));
        b = Arrays.equals(arregloInt, arregloIntLleno);
        System.out.printf("arregloInt %s arregloIntLleno\n", (b ? "==" : "!="));
    } // fin del método imprimirIgualdad

    @SuppressWarnings("empty-statement")
    public static void main(String args[]) {
        UsoArrays_paraAlumnos_Rellenar usoArreglos = new UsoArrays_paraAlumnos_Rellenar();
        usoArreglos.imprimirArreglos();
        usoArreglos.imprimirIgualdad();
        int ubicacion = usoArreglos.buscarUnInt(5);
        if (ubicacion >= 0) {
            System.out.printf("Se encontro el 5 en el elemento %d de arregloInt\n",
                    ubicacion);
        } else {
            System.out.println("No se encontro el 5 en arregloInt");
        }
        ubicacion = usoArreglos.buscarUnInt(8763);
        if (ubicacion >= 0) {
            System.out.printf("Se encontro el 8763 en el elemento %d en arregloInt\n", ubicacion);
        } else {
            System.out.println("No se encontro el 8763 en arregloInt");
        }
        ////////////////////////////
        //Añadido por luisnavarro... imaginemos que son arrays de miles de posiciones
        ////////////////////////////
        int[] misValores={7,2,5,9,1};
        int[] misValores3={7,3,6,9,6};
        int n=misValores.length;
        //Haz en misValores2 una copia de misValores
        int[] misValores2=Arrays.copyOf(misValores,misValores.length);
        System.out.println("Son misValores:"+Arrays.toString(misValores)+" y mis valores2:"+Arrays.toString(misValores2)+" iguales?"+Arrays.equals(misValores, misValores2));
        //Mira si misValores y misValores2 son iguales y si son el mismo
        System.out.println("");//misValores y misValores2 son iguales?"
        //("misValores y misValores2 son el mismo?"
        System.out.println("Son misValores:"+Arrays.toString(misValores)+" y mis valores2:"+Arrays.toString(misValores2)+" el mismo(la misma dirección de memoria, son variables que se refieren al mismo objeto?"+(misValores==misValores2));
        //Creo en misValores4 una variable que apunte a misValores
        int[] misValores4=misValores;
        //Mira si misValores y misValores4 son iguales y si son el mismo
        System.out.println("");
        System.out.println("");
        //ordena misValores2 e imprimo
      //  int[] misValores2Ordenado=Arrays.sort(misValores2);
      int[] misValores2Ordenado=Arrays.copyOf(misValores2, n);
      Arrays.sort(misValores2Ordenado);
        System.out.println("");
        //cuantas posiciones... haz una copia en misValores2 de las m primeras posiciones de misValores, si hay faltan lo rellenas a 0s. Mis valores2 ha de tener longitud m al final
        int m=9;
        misValores2=null;
        System.out.println("");
        //Imprimo la primera posición en que difieren misValores y misValores3
        n=misValores.length;
        int disc=0;
        System.out.println("Imprimo la primera posición en que difieren misValores y misValores3: "+disc);
        //Imprimo la primera posición en que difieren misValores y misValores3
        disc=0;
        System.out.println("Imprimo la primera posición en que difieren misValores y misValores3 a partir de la posición 3, relativa: "+disc);
        //Imprimo misValores, misValores2 misValores3 para ver cómo vamos
        System.out.println("Mis valores2="+Arrays.toString( misValores2));
        //Quiero crear un array que tenga los elementos comunes en la misma posición y los distintos un -1 entre misValores y misValores3 y respetando los arrays originales
        //a-usando los arrays del tema pasado
        //b-usando la clase Arrays, el método mismatch....imagina que es un array de miles de enteros que tienen pocos cambios
        int[] arrayResultado=null;//Arrays.copyOf(misValores, n);

        //hacer el de la carrera, parte de contar cuantas vueltas ha dado cada participante....pero con una sola pasada y sin declarar arrays adicionales
        //cojo array, lo ordeno y cuento cada participante cuantas vueltas con una sóla pasada al array...
        int[] dorsalesControlador={7, 3, 5, 5, 3, 1, 6, 2, 1, 1, 5, 2, 4, 1, 7, 5, 7, 6, 7, 5, 5, 6, 4, 3};
        //algunas pistas
        n=dorsalesControlador.length;
        int max=dorsalesControlador[n-1];
        
    } // fin de main
} // fin de la clase UsoArraysﬁ
