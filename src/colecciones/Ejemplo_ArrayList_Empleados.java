
package colecciones;

import java.util.ArrayList;
import java.util.Iterator;


public class Ejemplo_ArrayList_Empleados {

 
    public static void main(String[] args) {
        // TODO code application logic here
        //Empleado listaEmpleados[]=new Empleado[4];
        ArrayList <Empleado> listaEmpleados = new ArrayList<Empleado>();
        listaEmpleados.add(new Empleado("Ana",45,2500));
        listaEmpleados.add(new Empleado("Antonio",55,2000));
        listaEmpleados.add(new Empleado("María",25,2600));
        listaEmpleados.add(new Empleado("Juan",35,2100));
        listaEmpleados.add(new Empleado("Antonio",55,2000));
        //listaEmpleados.set(1,new Empleado("Olga",22,2200));
        System.out.printf("El número de elementos es: %d\n",listaEmpleados.size());
        
        //Recorrer ArrayList con objeto Iterator
        Iterator <Empleado> mi_iterador =listaEmpleados.iterator();
        while(mi_iterador.hasNext())
            System.out.println(mi_iterador.next().dameDatos());
        /*for(Empleado e:listaEmpleados)
            System.out.println(e.dameDatos());*/
        
        /*for(int i=0;i<listaEmpleados.size();i++){
            Empleado e = listaEmpleados.get(i);
            System.out.println(e.dameDatos());
        }*/
        //Vamos a ordenar la lista de empleados
        
        //Comparar 2 objetos
        Empleado empleado1 = new Empleado ("Miguel",20,2500);
        Empleado empleado2 = new Empleado ("Miguel",20,2500);
        if(empleado1.equals(empleado2)){//es de la clase Objects
            System.out.println("Los empleados son iguales");
            System.out.println(empleado1.hashCode());
            System.out.println(empleado2.hashCode());
        }
        else
            System.out.println ("Los empleados no son iguales");
        
    }  
}
