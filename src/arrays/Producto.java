/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

import java.util.Objects;

/**
 *
 * @author mpaniagua
 */
public class Producto {

    private String nombre;
    private float precio;

    public Producto() {
        this.nombre = "desconocido";
        this.precio = 0;
    }

    public Producto(String nombre, float precio) {
        this.setNombre(nombre);
        this.setPrecio(precio);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }

    public Producto(String nombre) {
        this.setNombre(nombre);
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return "Producto: " + nombre + " "+ precio;
    }

    public void setNombre(String nombre) {
        this.nombre = primeraEnMayuscula(nombre);
    }
    private String primeraEnMayuscula(String cadena){
      String primeraLetra = ""+cadena.charAt(0);
      String restoCadena = cadena.substring(1);
      return primeraLetra.toUpperCase()+restoCadena.toLowerCase();
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        if (precio < 0) {//No permitimos números negativos
            System.out.println("Precio negativo no permitido");
        } else {
            this.precio = precio;
        }
    }

}
