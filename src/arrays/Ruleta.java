/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

import java.util.Random;

/**
 *
 * @author mpaniagua
 */
public class Ruleta {

    String[] nombres = {
        "AAAlba Cabrera, Adri�n",
        "JBBerm�dez Matas, Javier",
        "Bujalance Rodr�guez, Julia",
        "SCCarrasco Plata, Samuel",
        "MCCobos Ruiz, Manuel",
        "PCC�rdoba Conde, Purificaci�n",
        "JCCorona D�az, Juan Antonio",
        "CEEspliguero Sardinero, Carlos Manuel",
        "AEEsquivel Barrera, Adri�n",
        "JGGordillo Horcas, Jon",
        "ALL�pez Gonz�lez, Antonio Jes�s",
        "JNNavarro Conti, Jos� Manuel",
        "AOOjeda Rey, Alberto",
        "Pascual Fern�ndez, H�ctor",
        "RSS�nchez Castillejo, Rafael",
        "RSS�nchez Mariano, Rafael",
        "FSSantana Ramos, Francisco Jos�",
        "JSSerrano Gonz�lez, Javier",
        "MSSfali Sihadi, Mohamed",
        "Silva G�mez, Claudia"
    };
    boolean[] hanJugado;
    Random aleatorio;
    static int jugados=0;

    public Ruleta() {
        hanJugado = new boolean[nombres.length];
        aleatorio = new Random();
        reiniciarJuego();
        
       
        
    }

    public void reiniciarJuego() {
        for (int i = 0; i < hanJugado.length; i++) {
            hanJugado[i] = false;
        }
        jugados=0;
    }

    public int dameAleatorio() {
        return aleatorio.nextInt(hanJugado.length);
    }

    public void jugar() {
        int posicionInicial = dameAleatorio();
        int contador = 0;
        while (hanJugado[posicionInicial] && contador < hanJugado.length) {
            contador++;
            posicionInicial = (posicionInicial + 1) % hanJugado.length;
        }
        if (contador != hanJugado.length) {
            System.out.println(jugados+"-Le toca presentar a: " + nombres[posicionInicial]);
            hanJugado[posicionInicial] = true;
            jugados++;
        } else {
            System.out.println("=====>Ya ha jugado todo el mundo. Reinicio la ruleta");
            this.reiniciarJuego();
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Ruleta ruleta = new Ruleta();

        for (int i = 0; i < 100; i++) {
            ruleta.jugar();
        }
    }

}
