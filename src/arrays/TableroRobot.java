/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

/**
 *
 * @author mpaniagua
 */
public class TableroRobot {

    private boolean[][] tablero;
    private String caracter = "X";

    //Creamos constructor
    public TableroRobot(int filas, int columnas) {
        this.tablero = new boolean[filas][columnas];
    }

    public String getCaracter() {
        return caracter;
    }

    public void setCaracter(String caracter) {
        this.caracter = caracter;
    }

    int getFilas() {
        return this.tablero.length;
    }

    int getColumnas() {
        return this.tablero[0].length;
    }

    public void dibujaCelda(int fila, int columna) {
        if (fila >= 0 && fila < getFilas() && columna >= 0 && columna < getColumnas()) {
            tablero[fila][columna] = true;
        }
    }

    public String toString() {
        String resultado = "";
        for(int i = 0; i< getColumnas()+2;i++){
         resultado+="_";
        }
        resultado+="\n";
        for (int i = 0; i < getFilas(); i++) {
            resultado += "|";
            for (int j = 0; j < getColumnas(); j++) {
                /*
                if (this.tablero[i][j]) {
                    resultado += "X";
                } else {
                    resultado += " ";
                }
                */
                resultado += (this.tablero[i][j] ? caracter : " ");
            }
            resultado += "|\n";
        }
        for(int i = 0; i< getColumnas()+2;i++){
         resultado+="¯";
        }
        resultado+="\n";
        return resultado;

    }

}
