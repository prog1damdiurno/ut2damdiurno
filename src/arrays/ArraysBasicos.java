/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

/**
 *
 * @author mpaniagua
 */
public class ArraysBasicos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*
        LocalDate fechaNi�o1 = LocalDate.of(2022, Month.MARCH, 14);
        LocalDate fechaNi�o2 = LocalDate.of(2019, 1, 14);
        
        Period periodo = Period.between(fechaNi�o2, fechaNi�o1);
        System.out.printf("Diferencia en %d a�os, %d meses %d d�as%n", 
                periodo.getYears(),
                periodo.getMonths(),
                periodo.getDays());
        
        long diferenciaDias = ChronoUnit.DAYS.between(fechaNi�o2, fechaNi�o1)*24*60*60;
        
        System.out.println("Diferencia en segundos: "+diferenciaDias);
         */
        int[] numeros = {5,7,7,8,15,12,8,7};
        
        Arrays.sort(numeros);
        int suma = 0;

        for (int i = 0; i < numeros.length; i++) {
            suma += numeros[i];
        }
        System.out.println("Suma total del array: " + suma);
        System.out.println("Promedio: " + (suma / numeros.length));

        int[] invertido = invierteArray(numeros);

        for (int i = 0; i < invertido.length; i++) {
            System.out.print(invertido[i] + " ");
        }
        invierteArray2(invertido);//Invierto el propio array
        System.out.println("Muestro el invertido del invertido");
        for (int i = 0; i < invertido.length; i++) {
            System.out.print(invertido[i] + " ");
        }
        System.out.println("El valor mayor es: "+dameMaximo(numeros));
        System.out.println("Cantida de repetidos: "+dameCuantosDuplicadosHay(numeros));
        System.out.println("Sin repetidos: "+Arrays.toString(sinDuplicados(numeros)));
        
        
        

    }

    static int[] copiarArray(int[] original) {

        int[] aux = new int[original.length];
        for (int i = 0; i < original.length; i++) {
            aux[i] = original[i];
        }
        return aux;
    }

    static void invierteArray2(int[] original) {
        int punteroInicio = 0;
        int punteroFinal = original.length - 1;
        int aux;
        do {
            aux = original[punteroInicio];
            original[punteroInicio] = original[punteroFinal];
            original[punteroFinal] = aux;
            punteroInicio++;
            punteroFinal--;
        } while (punteroInicio < punteroFinal);
    }

    static int[] invierteArray(int[] original) {

        int[] aux = new int[original.length];
        //int contador =0;
        for (int i = original.length - 1; i >= 0; i--) {
            aux[original.length - i - 1] = original[i];
            //contador++;
        }
        return aux;
    }

    static int dameMaximo(int[] arrayEnteros) {
        int maximo = arrayEnteros[0];
        for (int i = 0; i < arrayEnteros.length; i++) {
            if (arrayEnteros[i] > maximo) {
                maximo = arrayEnteros[i];
            }
        }
        return maximo;

    }
    static int dameCuantosDuplicadosHay(int[] arrayEntero){
        
        int cantidadRepetidos=0;
        int[] contador = new int[arrayEntero.length];
     
        for(int i=0; i<arrayEntero.length;i++){
         for (int j = i+1; j < arrayEntero.length; j++ ){
          if (arrayEntero[i]==arrayEntero[j] && contador[i]!=-1){
           contador[j]=-1;
           cantidadRepetidos++;
          }
         }
        }
        return cantidadRepetidos;
        
    }
    
    static int[] sinDuplicados(int[] arrayEntero){
       
       int cantidadRepetidos=0;
        int[] contador = new int[arrayEntero.length];
     
        for(int i=0; i<arrayEntero.length;i++){
         for (int j = i+1; j < arrayEntero.length; j++ ){
          if (arrayEntero[i]==arrayEntero[j] && contador[i]!=-1){
           contador[j]=-1;
           cantidadRepetidos++;
          }
         }
        }
        int longitudSinDuplicados = arrayEntero.length - cantidadRepetidos;
        int[] solucion = new int[longitudSinDuplicados];
        
        int j = 0;
        for (int i = 0; i< contador.length; i++){
           if (contador[i]!=-1){
               solucion[j]=arrayEntero[i];
               j++;
           }
        }
      
    return solucion;
    }

}
