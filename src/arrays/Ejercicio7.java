/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class Ejercicio7 {

    /**
     * @param args the command line arguments
     */
    Producto[] productos;

    public static void main(String[] args) {
        // TODO code application logic here
        int opcion;
        Scanner scanner = new Scanner(System.in);
        Ejercicio7 ejercicio7 = new Ejercicio7();
        boolean salir = false;
        ejercicio7.limpiarPantalla();
        do {
            opcion = ejercicio7.menu();
            switch (opcion) {
                case 1://Nuevo producto

                    System.out.print("Nombre: ");
                    String nombre = scanner.nextLine();
                    System.out.print("Precio: ");
                    float precio = scanner.nextFloat();
                    scanner.nextLine();
                    Producto nuevoProducto = new Producto(nombre, precio);
                    ejercicio7.insertarProducto(nuevoProducto);
                    break;
                case 2://Precio medio
                    System.out.println("El promedio es:"+ejercicio7.promedio());
                    break;
                case 3: //Listar producto
                    ejercicio7.listarProd();
                    break;
                case 4:
                    salir = true;
                    break;

            }
        } while (!salir);

    }

    void limpiarPantalla() {
        for (int i = 0; i < 25; i++) {
            System.out.println("");
        }
    }

    int menu() {
        Scanner scanner = new Scanner(System.in);
        int opcion = 0;
        System.out.println("1.Nuevo Producto");
        System.out.println("2.Precio Medio");
        System.out.println("3.Listar Productos");
        System.out.println("4.Salir");
        System.out.println("===================");
        System.out.print("Introduzca opción: ");
        opcion = scanner.nextInt();

        return opcion;
    }

    void insertarProducto(Producto p) {
        if (productos == null) {
            productos = new Producto[1];
            productos[0] = p;
        } else {
            Producto[] auxiliar = new Producto[productos.length + 1];//Creo array auxiliar con un elemento más
            for (int i = 0; i < productos.length; i++) { //Copio el array
                auxiliar[i] = productos[i];
            }
            auxiliar[productos.length] = p;//Añado el nuevo elemento
            this.productos = auxiliar;//Sustituyo el array antiguo por el nuevo con el nuevo producto
        }
    }

    void listarProd() {
        System.out.println("-------------------------------");

        if (this.productos != null) { //Listo si hay algún producto
            /*
            for (int i = 0; i < this.productos.length; i++) {
                System.out.println(this.productos[i]);
            }
             */
            for (Producto p : this.productos) {
                System.out.println(p);
            }

        }
        System.out.println("-------------------------------");

    }

    float promedio() {
        float suma = 0;
        for (Producto p : this.productos) {
            suma += p.getPrecio();
        }
        return suma / this.productos.length;
    }

}
