/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

/**
 *
 * @author mpaniagua
 */
public class Robots {

    public enum Direccion {
        NORTE, SUR, ESTE, OESTE;
    }

    public enum Orden {
        AVANZA, GIRA_DERECHA, DIBUJA;
    }
//Atributos de la clase
    private TableroRobot tablero;
    private int fila;
    private int columna;
    private Direccion direccion;

    public Robots(TableroRobot tablero, int fila, int columna) {
        this.tablero = tablero;
        this.fila = fila;
        this.columna = columna;
        this.direccion = Direccion.ESTE;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    private void avanza() {
        switch (direccion) {
            case NORTE:
                if (fila > 0) {
                    fila--;
                }
                break;
            case SUR:
                if (fila < tablero.getFilas() - 1) {
                    fila++;
                }
                break;
            case ESTE:
                if (columna < tablero.getColumnas() - 1) {
                    columna++;
                }
                break;
            case OESTE:
                if (columna > 0) {
                    columna--;
                }
                break;
        }
    }

    private void giraDerecha() {
        switch (direccion) {
            case NORTE:
                direccion = Direccion.ESTE;
                break;
            case SUR:
                direccion = Direccion.OESTE;
                break;
            case ESTE:
                direccion = Direccion.SUR;
                break;
            case OESTE:
                direccion = Direccion.NORTE;
                break;
        }
    }

    private void dibuja() {
        tablero.dibujaCelda(fila, columna);
    }

    public void ejecutaOrdenes(String ordenes) {
        int cantidad = 0;
        for (char orden : ordenes.toCharArray()) {
            switch (orden) {
                case 'A':
                    if (cantidad == 0) {
                        this.avanza();
                    } else {
                        for (int i = 0; i < cantidad; i++) {
                            this.avanza();
                        }
                        cantidad = 0;
                    }
                    break;
                case 'D':
                    if (cantidad==0){
                        this.dibuja();
                    }else{
                     for(int i =0; i < cantidad; i++){
                      this.dibuja();
                      this.avanza();
                     }
                     cantidad=0;
                    }
                    break;
                case 'G':
                    if (cantidad==0){
                    this.giraDerecha();
                    }else{
                     for(int i=0; i< cantidad; i++){
                      this.giraDerecha();
                     }
                    }
                    break;
                    default:
                        cantidad = Integer.parseInt(""+orden);
                    break;
            }
        }
    }

}
