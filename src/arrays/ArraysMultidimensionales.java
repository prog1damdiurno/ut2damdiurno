/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

/**
 *
 * @author mpaniagua
 */
public class ArraysMultidimensionales {

    public static void main(String[] args) {
        int[][] matriz = new int[3][2];
        int[][] matriz2 = {{1, 2}, {3, 4}, {4, 5, 6}};

        for (int i = 0; i < matriz2.length; i++) {
            for (int j = 0; j < matriz2[i].length; j++) {
                System.out.print(matriz2[i][j] + " ");

            }
            System.out.println("");
        }
        
        tablaMultiplicar();

    }

    public static void tablaMultiplicar() {
        final int FILAS = 10;
        final int COLUMNAS = 10;
        int[][] tablaMultiplicar = new int[FILAS][COLUMNAS];
        
        for (int i=0; i< FILAS; i++){
            for(int j=0;j<COLUMNAS; j++){
                tablaMultiplicar[i][j]= (i+1)*(j+1);
            }
        }
        
        System.out.println("Muestro mi tabla:");
        for (int i=0; i< FILAS; i++){
            System.out.println("La tabla del "+(i+1));
            for(int j=0;j<COLUMNAS; j++){
                System.out.print((i+1)+"x"+(j+1)+"="+tablaMultiplicar[i][j]+"\t");
            }
            System.out.println("");
        }
        

    }

}
