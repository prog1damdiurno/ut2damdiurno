/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arrays2;

import java.util.Arrays;

/**
 *
 * @author luisnavarro
 */
public class TeoriaUT5ene24 {
    public static void main(String[] args) {
        int[] valores={1,2,3,4,5};
        for (int i=0;i<valores.length;i++)
        {
            System.out.print("-"+valores[i]);
        }
        System.out.println("\n LeeBien1");
        leeBien1(valores);
        for (int i=0;i<valores.length;i++)
        {
            System.out.print("-"+valores[i]);
        }
        System.out.println("\n LeeBien2");
        valores=leeBien2();
        for (int i=0;i<valores.length;i++)
        {
            System.out.print("-"+valores[i]);
        }
        System.out.println("\n LeeMal");
        leeMal(valores);
        for (int i=0;i<valores.length;i++)
        {
            System.out.print("-"+valores[i]);
        }
                System.out.println("\n Intercambio posición 1 y 3");

        intercambiaPosiciones(valores, 1, 3);
        //int j=0;
        for (int j : valores){
            System.out.print("-"+j);            
        }
        System.out.println("");
        System.out.println(sumarEnteros(2,3,4));
        System.out.println(sumarEnteros(7,4));
    }
    public static void intercambiaPosiciones(int[] v, int i, int j)
    {
        int aux=v[i];
        v[i]=v[j];
        v[j]=aux;
    }
    public static int sumarEnteros(int...valores2){
        int suma=0;
        for (int i:valores2){
            suma+=i;
        }
        return suma;
    }
    public static void leeBien1(int[] v){
        v[0]=5;
        v[1]=4;
        v[2]=3;
        v[3]=2;
        v[4]=1;
        
    }
    public static int[] leeBien2(){
        int[] v=new int[5];
        v[0]=6;
        v[1]=7;
        v[2]=8;
        v[3]=9;
        v[4]=10;
        return v;
    }
    public static void leeMal(int v[]){
        v=new int[5];
        v[0]=11;
        v[1]=12;
        v[2]=13;
        v[3]=14;
        v[4]=15;
        
    }
}
