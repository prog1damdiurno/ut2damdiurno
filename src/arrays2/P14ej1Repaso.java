/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays2;

/**
 *
 * @author Luis Navarro
 */
public class P14ej1Repaso {
    public static void main(String[] args) {
        //"EJERCICIO 1

//  ---  DEFINICIÓN DE ARRAYS  ---  //
        //Array para apartado a
        int[] f = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        //Array para apartado b
        int[] g = new int[8];
        //Array para apartado c
        float[] c = new float[100];
        for (int i = 0; i < c.length; i++) {
            c[i] = 1;
        }
        //Arrays para apartado d
        int[] a = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] b = new int[34];
        //Array para apartado e
        float[] w = new float[99];
        for (int i = 0; i < w.length; i++) {
            w[i] = i;
        }

        //  ---  APARTADOS  ---  //
        //a)Muestra el valor del elemento 6 de un array f.
        System.out.println("Valor del elemento 6"  + f[6]);

        /*b)Inicializa los 5 primeros elementos de un array unidimensional 
        de enteros g a 8.*/
        for (int i = 0; i < 5; i++) {
            g[i] = 8;
        }

        //c)Total de los 100 elementos de punto-flotante de un array c.
        float total = 0.0f;
        for (int i = 0; i < c.length; i++) {
            total += c[i];
        }
        System.out.println("Total de los elementos: " + total);

        /*d)Copia los 11 elementos de un array a en la primera porción de 
        un array b, el cual contiene 34 elementos.*/
        for (int i = 0; i < a.length; i++) {
            b[i] = a[i];
        }

        /*e)Calcula y muestra el valor mayor y menor contenidos en un array 
        w de 99 elementos de punto-flotante.*/
        float mayor = w[0];//Float.MIN_VALUE;
        float menor = Float.MAX_VALUE;
        for (int i = 0; i < w.length; i++) {
            if (w[i] > mayor) {
                mayor = w[i];
            }
            if (w[i] < menor) {
                menor = w[i];
            }
        }
        System.out.println("Valor mayor: " + mayor);
        System.out.println("Valor menor: " + menor);

    }
}

   