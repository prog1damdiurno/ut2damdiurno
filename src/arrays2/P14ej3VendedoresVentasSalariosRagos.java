package arrays2;
import java.util.Arrays;
import utilidades.ES;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Luis Navarro
 */
public class P14ej3VendedoresVentasSalariosRagos {
    final static int MAXIMO_VENDEDORES=10;
    public static void main(String[] args) {
        //1. Leo ventas
        int ventas=0;
        new String();
        //new int[MAXIMO_VENDEDORES];
        int[] arrayVentas=new int[MAXIMO_VENDEDORES];
        int[] rangosEnLosQueCaenLosComerciales=new int[9];
        int contador=0;
        do{
            
            ventas=ES.leeEntero("MEta las ventas de cada vendedor, negativo para terminar");
            if (ventas>=0) arrayVentas[contador++]=ventas;
        }while((ventas>=0)&&(contador<MAXIMO_VENDEDORES));
        for (int i = 0; i < contador; i++) {
            System.out.print(arrayVentas[i]+"-");
        }
        System.out.println("");
        System.out.println(Arrays.toString(arrayVentas));
        
        //2. ORganizo en rangos
        double salario=0;
        int indiceDelRangoEnElQueCae;
        for (int i = 0; i < contador; i++) {
            salario=200+0.10*arrayVentas[i];
            //(int)(salario-200)/100
            indiceDelRangoEnElQueCae=8;
            /*switch (((salario>=200)&&(salario<300))?0:((salario>=300)&&(salario<400))?1: {
                case 0:
                case 1:
            }*/
            for (int j=0;j<8;j++)
                if ((salario>=200+j*100)&&(salario<300+j*100)) indiceDelRangoEnElQueCae=j;
            //supongamos que he calculado el rango en el que cae
            rangosEnLosQueCaenLosComerciales[indiceDelRangoEnElQueCae]++;
        }
        System.out.println(Arrays.toString(rangosEnLosQueCaenLosComerciales));
    }
}
