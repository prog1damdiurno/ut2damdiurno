/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forwhiledowhile;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class ejercicio6 {

    public static void main(String[] args) {
        int numeroInicio, numeroFinal;
        Scanner s = new Scanner(System.in);
        System.out.print("Dame el inicio de la cuenta: ");
        numeroInicio = s.nextInt();
        System.out.print("Dame el final de la cuenta: ");
        numeroFinal = s.nextInt();
        if (numeroInicio < numeroFinal) {
            for (int i = numeroInicio; i <= numeroFinal; i++) {
                System.out.println(i);
            }
        } else {
            for (int i = numeroInicio; i >= numeroFinal; i--) {
                System.out.println(i);
            }
        }

    }

}
