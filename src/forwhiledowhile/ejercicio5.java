/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forwhiledowhile;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 * Ejercicio que pide n�meros hasta que sea 0 y devueve la suma de los dos �ltimos
 * 
 */
public class ejercicio5 {
    public static void main(String[] args) {
       int numero1, numero2;
       Scanner s =new Scanner(System.in);
       
       System.out.print("Introduce un n�mero (0 para salir): ");
       numero1 = s.nextInt();
       
       while(numero1 != 0){
            System.out.print("Introduce un n�mero (0 para salir): ");
            numero2 = s.nextInt();
            System.out.println("La suma de los dos �ltimos es: "+(numero1+numero2));
            numero1 = numero2;
      }
 
    }
    
       
}
