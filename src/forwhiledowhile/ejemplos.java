/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forwhiledowhile;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class ejemplos {

    public static void main(String[] args) {

        float numero, suma = 0, media, cantidad = 0;
        Scanner s = new Scanner(System.in);

        System.out.print("Dame un número: ");
        numero = s.nextInt();

        while (numero >= 0) {
            suma += numero; //suma = suma + numero;
            cantidad++; //cantidad = cantidad + 1;
            System.out.print("Dame un número: ");
            numero = s.nextFloat();
        }
        media = suma / cantidad;
        System.out.println("La media de " + suma + " entre " + cantidad + " = " + media);

        //Mismo con do{}while
        suma = 0;
        cantidad = 0;

        do {
            System.out.print("Dame un número: ");
            numero = s.nextInt();
            if (numero<0){
            break;
            }
            suma += numero; //suma = suma + numero;
            cantidad++; //cantidad = cantidad + 1;
        } while (numero >= 0);
        media = suma / cantidad;
        System.out.println("La media de " + suma + " entre " + cantidad + " = " + media);

    
        for(int i=0; i<100;i++){
            System.out.println("Hola Mundo " + i);
            if (i>50){continue;}
            System.out.println("Adiós mundo+ " +i);
                    
        }
    
    
    }

}
