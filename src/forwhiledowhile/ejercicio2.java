/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forwhiledowhile;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class ejercicio2 {

    public static void main(String[] args) {
        int ancho, alto;
        Scanner s = new Scanner(System.in);

        System.out.print("Dame ancho: ");
        ancho = s.nextInt();
        System.out.print("Dame alto: ");
        alto = s.nextInt();
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                if(i==0 || j==0 || j == ancho -1 || i == alto-1){
                    System.out.print("*");
                }else{
                    System.out.print(" ");
                 }
            }
            System.out.print("\n");
        }

    }

}
