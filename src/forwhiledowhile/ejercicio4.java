/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forwhiledowhile;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class ejercicio4 {

    public static void main(String[] args) {
        int numero;
        Scanner s = new Scanner(System.in);
        do {
            System.out.print("Introduce un n�mero impar: ");
            numero = s.nextInt();
        } while (numero % 2 == 0);

        for (int y = 0; y < numero; y++) {
            for (int x = 0; x < numero; x++) {
                if (y == numero / 2 || x == numero /2 || 
                        (x >= (numero/2)-y && x<= (numero/2)+y && y<= numero/2) ||
                        (x <= (numero/2)+ (numero-y-1) && x>= (numero/2)-(numero-y-1) && y> numero/2)
                        
                        ) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }

    }
}
