/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class ejercicio3Examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int dado1, dado2;
        Scanner s = new Scanner(System.in);
        System.out.print("Cu�nto apuestas? ");
        int apuesta = s.nextInt();
        
        while(apuesta !=0){
            System.out.println("Tu saldo es de: "+apuesta);
            //Tiro los dados
            dado1 = tirarDados();
            dado2 = tirarDados();
            System.out.printf("Has sacado %d y %d (Total: %d)%n",dado1,dado2,(dado1+dado2));
            switch(dado1+dado2){
                case 7:
                case 11:
                    apuesta*=2;
                    break;
                case 2:
                case 3:
                case 12:
                    apuesta =0;
                    break;
                default:
                    System.out.println("Sigue jugando...");
            }
        }
        System.out.println("Lo has perdido todo");
    }

    static int tirarDados() {
        Random r = new Random();
        return r.nextInt(6) + 1;
    }

}
