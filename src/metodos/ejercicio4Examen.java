/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

/**
 *
 * @author mpaniagua
 */
public class ejercicio4Examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int resultado = dameElMasCool("Fulanoeee", "Trontanz");

        System.out.println("Resultado: " + resultado);

    }

    static int dameElMasCool(String nombre1, String nombre2) {
        int puntuacionNombre1 = 0, puntuacionNombre2 = 0;
        if (nombre1.length() > nombre2.length()) {
            puntuacionNombre1 = 2;
        } else if (nombre1.length() < nombre2.length()) {
            puntuacionNombre2 = 2;
        }
        puntuacionNombre1 += damePuntos(nombre1);
        System.out.println("Puntos nomber1: " + puntuacionNombre1);
        puntuacionNombre2 += damePuntos(nombre2);
        System.out.println("Puntos nombre2: " + puntuacionNombre2);
        if (puntuacionNombre1 > puntuacionNombre2) {
            return 1;
        }
        if (puntuacionNombre1 < puntuacionNombre2) {
            return 2;
        }
        return 0;
    }

    static int damePuntos(String nombre) {
        int puntos = 0;
        nombre = nombre.toLowerCase();

        for (int i = 0; i < nombre.length(); i++) {
            puntos += (nombre.charAt(i) == 'e') ? 1 : 0;
            puntos += (nombre.charAt(i) == 'z') ? 5 : 0;
        }
        return puntos;
    }
}
