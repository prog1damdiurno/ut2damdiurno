/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

/**
 *
 * @author mpaniagua
 */
public class ProbandoString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String str1 = "Hola, mundo"; // Crear una cadena literal
        String str2 = new String("Hola, mundo"); // Crear una cadena utilizando el constructor
        int length = str1.length(); // Obtiene la longitud de la cadena
        System.out.println("Longitud es: " + length);
        char charAt = str1.charAt(2); // Obtiene el car�cter en la posici�n 0 (�ndice basado en 0)
        System.out.println("El caracter es: " + charAt);

        for (int i = 0; i < str1.length(); i++) {
            System.out.println("" + str1.charAt(i));
        }
        int indexOf = str1.indexOf("las"); // Encuentra la primera aparici�n de "mundo"

        System.out.println("est� en: " + indexOf);

        boolean contains = str1.contains("Hola");
        //boolean contains = str1.indexOf("Hola")>=0?true:false;
        String substring = str1.substring(6, 9); // Obtiene una subcadena desde el �ndice 0 al 3
        System.out.println("Contiene: " + contains);

        System.out.println(substring);
        String replace = str1.replace("o", "u"); // Reemplaza "mundo" con "Java"

        System.out.println(replace);
        String[] split = str1.split(","); // Divide la cadena en un array de subcadenas

        boolean isEmpty = str1.isEmpty(); // Comprueba si la cadena est� vac�a
        String trimmed = str1.trim(); // Elimina espacios en blanco al principio y al final
        String toUpperCase = str1.toUpperCase(); // Convierte la cadena a may�sculas
        System.out.println(toUpperCase);

        String toLowerCase = str1.toLowerCase(); // Convierte la cadena a min�sculas
        boolean startsWith = str1.startsWith("Hola"); // Comprueba si la cadena comienza con "Hola"
        boolean endsWith = str1.endsWith("mundo"); // Comprueba si la cadena termina con "mundo"

        boolean equals = str1.equals(str2); // Compara dos cadenas por contenido
        boolean equalsIgnoreCase = str1.equalsIgnoreCase("hola, mundo"); // Compara ignorando may�sculas/min�sculas
        int compareTo = str1.compareTo("Adi�s"); // Compara lexicogr�ficamente

    }

}
