/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class EjemplosMetodos {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        /*
        int valor = 5;
        int valorDoble;

        valorDoble = dameDoble(valor);

        System.out.println("valorDoble: " + valorDoble);

        int base = 6;
        int altura = 5;
        int �rea;

        �rea = areaRect�ngulo(base, altura);

        System.out.println("�rea: " + �rea);

        float baseT = 7;
        float alturaT = 13;
        float areaT;

        areaT = �reaTri�ngulo(baseT, alturaT);
        System.out.println("El �rea del tri�ngulo es:" + areaT);

        dibujaCuadrado(3);
        dibujaCuadrado(15);
        dibujaCuadrado(8);
         */
 /*
        System.out.println("" + exponente(2, 4));
        System.out.println("" + Math.pow(2, 10));

        System.out.println("Dame tres n�meros");
        System.out.print("Primer n�mero: ");
        float numero1 = s.nextFloat();
        System.out.print("Segund n�mero: ");
        float numero2 = s.nextFloat();
        System.out.print("Tercer n�mero: ");
        float numero3 = s.nextFloat();
        float menor = dameElM�sPeque�o(numero1, numero2, numero3);
        System.out.println("El menor es: " + menor);
        float promedio = promedioDeTres(numero1, numero2, numero3);
        System.out.printf("El promedio de los es: %.1f", promedio );
         
        System.out.print("Dame una cadena: ");
        String cadena = s.nextLine();
        System.out.println("El mediod de la cadena es: " + dameMedio(cadena));
         
        System.out.print("Dame una cadena: ");
        String cadena = s.nextLine();
        System.out.println("N� de vocales: " + contarVocales(cadena));
        System.out.println("N� de vocales: " + contarVocalesV2(cadena));
         */
        System.out.print("Dame un n�mero: ");
        int numero = s.nextInt();
        int sumaDigitos = ejercicio6ContarDigitos(numero);
        System.out.println("La suma de los d�gitos es: " + sumaDigitos);
        String nombre = "JUAN";
        String enMinuscula = nombre.toLowerCase();
        System.out.println(nombre + " " + enMinuscula);
        
        System.out.println(subCadenaRecursivo("Marbella", 2, 4));

    }

    static int dameDoble(int x) {
        return x + x;
    }

    static int areaRect�ngulo(int base, int altura) {
        return base * altura;
    }

    static float �reaTri�ngulo(float base, float altura) {
        return base * altura / 2;
    }

    static void dibujaCuadrado(int lado) {
        for (int i = 0; i < lado; i++) {
            for (int j = 0; j < lado; j++) {
                if (i == 0 || j == 0 || i == lado - 1 || j == lado - 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }
    }

    /*
    static int exponente(int base, int exponente) {
        int solucion = 1;
        for (int i = 0; i < exponente; i++) {
            solucion *= base;
        }
        return solucion;
}*/
    static int exponente(int base, int exponente) {
        //Soluci�n recursiva
        if (exponente == 0) {
            return 1;
        } else {
            return base * exponente(base, exponente - 1);
        }
    }

    static float dameElM�sPeque�o(float n1, float n2, float n3) {
        float menor;
        if (n1 <= n2 && n1 <= n3) {
            menor = n1;
        } else if (n2 <= n1 && n2 <= n3) {
            menor = n2;
        } else {
            menor = n3;
        }
        return menor;
    }

    static float promedioDeTres(float n1, float n2, float n3) {
        return (n1 + n2 + n3) / 3;

    }

    static String dameMedio(String cadena) {

        if (cadena.length() % 2 == 0) { //Es par devuelve 2 caracteres
            return cadena.substring(cadena.length() / 2 - 1, cadena.length() / 2 + 1);
        } else { //Es impar solo devuelvo un car�cter
            return "" + cadena.charAt(cadena.length() / 2);
        }

    }

    static int contarVocales(String cadena) {
        int contador = 0;
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == 'a'
                    || cadena.charAt(i) == 'e'
                    || cadena.charAt(i) == 'i'
                    || cadena.charAt(i) == 'o'
                    || cadena.charAt(i) == 'u') {
                contador++;
            }
        }
        return contador;
    }

    static int contarVocalesV2(String cadena) {
        int contador = 0;
        String vocales = "aeiouAEIOU";
        for (int i = 0; i < cadena.length(); i++) {
            if (vocales.contains("" + cadena.charAt(i))) {
                contador++;
            }
        }
        return contador;
    }

    static int ejercicio6ContarDigitos(int numero) {
        //Soluci�n recursiva
        if (numero < 10) {
            return numero;
        } else {
            return (numero % 10) + ejercicio6ContarDigitos(numero / 10);
        }

    }

    static boolean isDigit(String caracter) {
        String digitos = "01235456789";
        return digitos.contains(caracter);

    }
    static String subCadenaRecursivo(String cadena, int pos, int cantidad){
        if (cantidad == 1){
            return ""+cadena.charAt(pos);
        }else{
        return ""+cadena.charAt(pos)+subCadenaRecursivo(cadena, pos+1, cantidad-1); 
        }
        
    }

}
