/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

import java.util.Date;
import metodos.EjemplosRandom;

public class EjemplosPrintf {

    public static void main(String[] args) {
        // Impresi�n de cadenas con n�meros
        String nombre = "Juan";
        String apellidos = "Garc�a";
        int edad = 25;
        System.out.println("Hola, " + nombre + " " + apellidos + ". Tienes " + edad + " a�os");

        System.out.printf("Hola, %s %s. Tienes %d a�os.%n", nombre, apellidos, edad);

        // Formateo de cadenas simples
        String producto = "Camiseta";
        double precio = 19.99;
        String producto2 = "Pantalones";
        double precio2 = 115.3;

        System.out.printf("%-15s: $%.2f%n", producto, precio);
        System.out.printf("%-15s: $%.2f%n", producto2, precio2);

        // Redondeo de n�meros
        double numero = 123.456789;
        System.out.println("Redondeado " + (Math.round(numero * 100f) / 100f ));

        System.out.printf("N�mero redondeado: %.2f%n", numero);

        // N�meros en formato hexadecimal y octal
        int decimal = 255;
        System.out.printf("Decimal: %d, Hexadecimal: %X, Octal: %o%n", decimal, decimal, decimal);

        // Longitud y alineaci�n de campos
        String nombre1 = "Mar�a";
        int edad1 = 30;
        double peso1 = 65.5;
        String nombre2 = "Pedro";
        int edad2 = 28;
        double peso2 = 75.2;
        String formato = "Nombre: %-10s, Edad: %d, Peso: %.2f%n";
        System.out.printf(formato, nombre1, edad1, peso1);
        System.out.printf(formato, nombre2, edad2, peso2);

        // Formato de fecha y hora
        Date fecha = new Date();
        System.out.printf("Hoy es %tF %tT%n", fecha, fecha);
        
        System.out.println((char)66);
        
        for(int i =0; i<100;i++){
            System.out.print(EjemplosRandom.dameLetraAleatoria());
    }
        
        
    }
    
    static int menor(int numero1, int numero2){
     /*
        if (numero1< numero2){
      return numero1;
     }else {
      return numero2;
     }*/
     return numero1>numero2?numero2:numero1;
     
    }
}
