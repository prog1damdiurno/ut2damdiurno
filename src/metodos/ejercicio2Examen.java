/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

/**
 *
 * @author mpaniagua
 */
public class ejercicio2Examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        collatzR(227);
    }

    static void collatz(int numero) {
        while (numero != 1) {
            System.out.print(numero + " ");
            numero = (numero % 2 == 0) ? (numero / 2) : (numero * 3 + 1);
        }
        System.out.print("1");
    }

    static void collatzR(int numero) {
        if (numero == 1) {
            System.out.println("1");
        } else {
            System.out.print(numero + " ");
            if (numero % 2 == 0) {
                collatzR(numero / 2);
            } else {
                collatzR(numero * 3 + 1);
            }
        }
    }

}
