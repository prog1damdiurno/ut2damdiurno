/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

import java.util.Random;

/**
 *
 * @author mpaniagua
 */
public class EjemplosRandom {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        for (int i = 0; i < 10; i++) {
            int aleatorio = dameAleatorioEntreDosNumeros(5, 15);
            System.out.println("N� Aleatorio: " + aleatorio);
        }
        Random generadorAleatorio = new Random();
        System.out.println("-------------");

        for (int i = 0; i < 10; i++) {
            int aleatorio = generadorAleatorio.nextInt();
            System.out.println("Generado con Random: " + aleatorio);
        }
        System.out.println("-----------");

        for (int i = 0; i < 30; i++) {
            int aleatorio = generadorAleatorio.nextInt(11) + 5;
            System.out.println("Generado con Random: " + aleatorio);
        }
        System.out.println("-----------");

        for (int i = 0; i < 10; i++) {
            int aleatorio = dameAleatorioEntreDosNumerosUsandoRandom(5, 15);
            System.out.println("N� Aleatorio: " + aleatorio);
        }
        System.out.println("-----------");

        for (int i = 0; i < 10; i++) {
            char aleatorio = dameLetraAleatoria();
            System.out.print(aleatorio);
        }
    }

    static int dameAleatorioEntre1y10() {
        return (int) (1 + (Math.random() * 10)); //cast a entero
    }

    static int dameAleatorioEntreDosNumeros(int minimo, int maximo) {
        return minimo + (int) (Math.random() * ((maximo - minimo) + 1));
    }

    static int dameAleatorioEntreDosNumerosUsandoRandom(int minimo, int maximo) {
        Random r = new Random();
        return r.nextInt(maximo - minimo + 1) + minimo;
    }

    static char dameLetraAleatoria() {
        Random r = new Random();
        String letras = "ABCDEFGHIJKLMN�OPQRSTUVWXYZ";
        return letras.charAt(r.nextInt(letras.length()));
    }

}
