/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sentenciasif;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class ejercicio3 {

    public static void main(String[] args) {
        int numero1, numero2, resultado;
        String operacion;

        Scanner s = new Scanner(System.in);

        System.out.print("Dame número 1: ");
        numero1 = s.nextInt();
        System.out.print("Dame número 2: ");
        numero2 = s.nextInt();
        System.out.print("Operación: ");
        s.nextLine(); //Para poder tomar la operación
        operacion = s.nextLine();
        
        resultado = 0;
        switch (operacion) {
            case "+":
                resultado = numero1 + numero2;
                break;
            case "-":
                resultado = numero1 - numero2;
                break;
            case "*":
                resultado = numero1 * numero2;
                break;
            case "/":
                resultado = numero1 / numero2;
                break;
            default:
                System.out.println("Operación desconocida");
        }

        System.out.println("Resultado: " + resultado);

    }
}
