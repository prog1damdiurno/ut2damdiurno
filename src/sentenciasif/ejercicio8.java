/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sentenciasif;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class ejercicio8 {

    public static void main(String[] args) {
        int hora;
        String saludo="";

        Scanner s = new Scanner(System.in);

        System.out.println("Dame hora: ");
        hora = s.nextInt();

        if (hora >= 6 && hora <= 12) {
            saludo = "Buenos días";
        } else if (hora >= 13 && hora <= 20) {
            saludo = "Buenas tardes";
        } else  {
            saludo = "Buenas noches";
        }

        System.out.println(saludo);

    }
}
