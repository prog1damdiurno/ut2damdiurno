/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sentenciasif;

import java.util.Scanner;

/**
 *
 * @author mpaniagua
 */
public class diasemana {
     public static void main(String[] args) {
       int diaSemana;
       Scanner s =new Scanner(System.in);
       
        System.out.print("Dame un número: ");
        diaSemana = s.nextInt();
        
        /*
        if (diaSemana==1){
            System.out.println("Lunes");}
        else if (diaSemana==2){
            System.out.println("Martes");}...
*/
        String dia="";
        switch(diaSemana){
            case 1: dia="lunes";break;
            case 2: dia="martes";break;
            case 3: dia="miércoles";break;
            case 4: dia="jueves";break;
            case 5: dia="viernes";break;
            case 6: dia="sábado";break;
            case 7: dia="domingo";break;
            default: dia = "No existe";
        }
        System.out.println(dia);
        System.out.println("FIN");
        
    }
    
}
