/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.time.LocalDate;
import objetos.Personas.Estado;
import objetos.Personas.Genero;

/**
 *
 * @author mpaniagua
 */
public class PruebaClases {

    public static void main(String[] args) {
        Equipos cordoba = new Equipos("Cordoba A");
        Equipos sevilla = new Equipos("Sevilla", 5, 22,true, LocalDate.of(2022, 11, 11));
        
        System.out.println(cordoba);
        System.out.println(sevilla);
        
        System.out.println("D�as que lleva creado el Sevilla: "+sevilla.diasDesdeSuCreacion());

        System.out.println("D�as que lleva creado el Sevilla: "+sevilla.diasDesdeSuCreacionV2());
 
        System.out.println("EQuipos totales:"+Equipos.getNumeroEquipos());
          
        
        for (int i=0; i<10;i++){
            System.out.printf("Fecha %d: %s%n",i,Equipos.dameFechaAleatoria());
        }
        
        sevilla.dibujaAdorno(6, 20);
        
        
        
        /*
        Personas maria = new Personas("Mar�a", 55, Personas.Genero.FEMENINO, Personas.Estado.JUBILADO);

        maria.saludar();
        maria.setNombre("Luisa");
        maria.saludar();
        //System.out.println("Cantidad de personas: "+ Personas.cantidad);
        Personas adrian = new Personas("Adri�n", 15, Genero.MASCULINO, Estado.ESTUDIANTE);
 
        Personas juan = new Personas("Juan");
        juan.setEdad(20);
        
        System.out.printf("La edad de juan es %d%n", juan.getEdad());
        juan.saludar();
        adrian.saludar();

        adrian.cumplirA�os();
        adrian.saludar();

        Personas p1 = new Personas();
        p1.saludar();
        
        System.out.println("Cantidad de personas: "+ adrian.dameCantidadPersonas());

        p1.cambiarCantidadPersonas(100);
        
        System.out.println("Cantidad de personas: "+ Personas.dameCantidadPersonas());

*/
        

    }

}
