/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author mpaniagua
 */
public class Personas {

    private static int cantidad = 0;

    @Override
    public String toString() {
        return "Personas{" + "edad=" + edad + ", nombre=" + nombre + ", genero=" + genero + ", estado=" + estado + '}';
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        if (edad < 0) {
            System.out.println("Edad no v�lida");
            this.edad = 0;
            return;
        }
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    //Defino los atributos
    private int edad;
    private String nombre;
    private Genero genero;
    private Estado estado;

    public enum Estado {
        ESTUDIANTE,
        TRABAJADOR,
        JUBILADO,
        PARADO
    }

    public enum Contenido {
        A, M, B
    }

    public enum Genero {
        MASCULINO,
        FEMENINO,
        OTRO
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*
        Personas adrian = new Personas("Adri�n", 15, Genero.MASCULINO, Estado.ESTUDIANTE);

        Personas juan = new Personas("Juan");
        juan.setEdad(20);
        
        System.out.printf("La edad de juan es %d%n", juan.getEdad());
        juan.saludar();
        adrian.saludar();

        adrian.cumplirA�os();
        adrian.saludar();

        Personas p1 = new Personas();
        p1.saludar();
        Personas maria = new Personas("Mar�a", 55, Genero.FEMENINO, Estado.JUBILADO);

        System.out.println("Es Mar�a mayor que Juan?:" + maria.esMayor(juan));

        sumarUno(maria);

        maria.saludar();

        int valor = 1;
         */
        int valor = 1;
        sumarUno(valor);

        System.out.println("Valor: " + valor);

        Personas maria = new Personas("Mar�a", 55, Genero.FEMENINO, Estado.JUBILADO);
        sumarUno(maria);
        maria.saludar();
        maria.nombre = "Luisa";

    }

    //Constructor
    public Personas(String nombreaux, int edadaux, Genero genero, Estado estado) {
        this.edad = edadaux;
        this.nombre = nombreaux;
        this.genero = genero;
        this.estado = estado;
        this.cantidad++;

    }
    //Sobrecarga el constructor

    public Personas(String nombre) {
        this.nombre = nombre;
        this.edad = 18;
        this.genero = Genero.OTRO;
        this.estado = Estado.PARADO;
        this.cantidad++;

    }

    public Personas() {
        this.nombre = "Desconocido";
        this.edad = 1;
        this.genero = Genero.OTRO;
        this.estado = Estado.PARADO;
        this.cantidad++;

    }

    //m�todos
    String dameGenero() {
        String generoEnCadena = "";
        switch (this.genero) {
            case MASCULINO:
                generoEnCadena = "hombre";
                break;
            case FEMENINO:
                generoEnCadena = "mujer";
                break;
            case OTRO:
                generoEnCadena = "no binario";
                break;

        }
        return generoEnCadena;
    }

    static int dameCantidadPersonas() {

        return cantidad;
    }

    static void cambiarCantidadPersonas(int can) {
        cantidad = can;
    }

    String dameEstado() {
        String estadoEnCadena = "";
        switch (this.estado) {
            case ESTUDIANTE:
                estadoEnCadena = "estudiando";
                break;
            case JUBILADO:
                estadoEnCadena = "jubilado";
                break;
            case PARADO:
                estadoEnCadena = "buscando trabajo";
                break;
            case TRABAJADOR:
                estadoEnCadena = "trabajando";
                break;
        }
        return estadoEnCadena;
    }

    public void saludar() {
        System.out.printf("Mi nombre es %s, tengo %d a�os. Soy %s. Estoy %s. Encantado/a%n", this.nombre, edad, this.dameGenero(), this.dameEstado());
    }

    public void cumplirA�os() {
        this.edad++;
    }

    public boolean esHombre() {
        return this.genero == Genero.MASCULINO;
    }

    public boolean esMayor(Personas p) {
        return this.edad > p.edad;
    }

    static void sumarUno(int valor) {
        valor++;
        System.out.println("Valor dentro del m�todo: " + valor);

    }

    static void sumarUno(Personas p) {
        p.cumplirA�os();
    }

}
