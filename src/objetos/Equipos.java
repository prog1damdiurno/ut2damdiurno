/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.Random;

/**
 *
 * @author mpaniagua
 */
public class Equipos {
    private String nombre;
    private int puntuación;
    private int numeroJugadores;
    private boolean debeDinero;
    private LocalDate fechaCreación;
    private static int numeroEquipos;

    public Equipos(String nombre, int puntuación, int numeroJugadores, boolean debeDinero, LocalDate fechaCreación) {
        this.nombre = nombre;
        this.puntuación = puntuación;
        this.numeroJugadores = numeroJugadores;
        this.debeDinero = debeDinero;
        this.fechaCreación = fechaCreación;
        this.numeroEquipos++;
    }

    public Equipos(String nombre) {
        this.nombre = nombre;
        this.puntuación = 0;
        this.numeroJugadores = 11;
        this.debeDinero = false;
        this.fechaCreación = LocalDate.now();
        this.numeroEquipos++;
    }
    
    public int diasDesdeSuCreacion(){
     LocalDate hoy = LocalDate.now();
     int diasDeDiferencia = (int) ChronoUnit.DAYS.between(this.getFechaCreación(),hoy);
     return diasDeDiferencia;
    }
    public int diasDesdeSuCreacionV2(){
     LocalDate hoy = LocalDate.now();
     Period periodo = Period.between(this.getFechaCreación(),hoy);
     return periodo.getDays();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntuación() {
        return puntuación;
    }

    public void setPuntuación(int puntuación) {
        this.puntuación = puntuación;
    }

    public int getNumeroJugadores() {
        return numeroJugadores;
    }

    public void setNumeroJugadores(int numeroJugadores) {
        this.numeroJugadores = numeroJugadores;
    }

    public boolean isDebeDinero() {
        return debeDinero;
    }

    public void setDebeDinero(boolean debeDinero) {
        this.debeDinero = debeDinero;
    }

    public LocalDate getFechaCreación() {
        return fechaCreación;
    }

    public void setFechaCreación(LocalDate fechaCreación) {
        this.fechaCreación = fechaCreación;
    }

    public static int getNumeroEquipos() {
        return numeroEquipos;
    }

    public static void setNumeroEquipos(int numeroEquipos) {
        Equipos.numeroEquipos = numeroEquipos;
    }

    @Override
    public String toString() {
        return "Equipos{" + "nombre=" + nombre + ", puntuaci\u00f3n=" + puntuación + ", numeroJugadores=" + numeroJugadores + ", debeDinero=" + debeDinero + ", fechaCreaci\u00f3n=" + fechaCreación + '}';
    }

    
    public boolean equals(Equipos obj) {
        if (!this.getNombre().equals(obj.getNombre())){
            return false;
        }
        if (Math.abs(this.getPuntuación()-obj.getPuntuación())>5){
            return false;
        }
        return true;
    }
    
    public static String dameFechaAleatoria(){
        LocalDate hoy = LocalDate.now();
        LocalDate referencia = LocalDate.of(1980, 1, 1);
        long dias = ChronoUnit.DAYS.between(referencia, hoy);
        
        Random random = new Random();
        int diasAleatorios = random.nextInt((int)dias);
        LocalDate fechaAleatorio = referencia.plusDays(diasAleatorios);
        
        return fechaAleatorio.toString();
        
    }
    
    void dibujaAdorno(int ancho, int largo){
    
        for(int i =0; i<largo;i++){
            
            
            
            for (int j = 0; j < ancho; j++) {
                for(int k=0;k<=j; k++){
                    System.out.print("*");
                }
                System.out.println("");
            }
            
        }
        
    }
    
    
    
    
    
    
}
