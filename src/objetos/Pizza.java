/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author mpaniagua
 */
enum Tama�o {
    MEDIANA("Mediana"), FAMILIAR("Familiar");
    private String traducci�n;

    private Tama�o(String t) {
        this.traducci�n = t;
    }

    @Override
    public String toString() {
        return this.traducci�n;
    }

}

enum Tipo {
    MARGARITA("Margarita"), CUATRO_QUESOS("Cuatro Quesos"), FUNGHI("Funghi");
    private String traducci�n;

    private Tipo(String t) {
        this.traducci�n = t;
    }
    @Override
    public String toString() {
        return this.traducci�n;
    }

}

enum Estado {
    PEDIDA, SERVIDA;
}

public class Pizza {

    Tama�o tama�o;
    Tipo tipo;
    Estado estado;
    static int contadorPedidas;
    static int contadorServidas;

    @Override
    public String toString() {
        return "Pizza{" + "tama\u00f1o=" + tama�o + ", tipo=" + tipo + ", estado=" + estado + '}';
    }

    public Pizza(Tipo tipo,Tama�o tama�o) {
        this.tama�o = tama�o;
        this.tipo = tipo;
        this.estado = Estado.PEDIDA;
    }
    
    

}
